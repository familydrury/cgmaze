// render level
var lvl = 'level1';
var isFinished = false;

var currentMap = maps[lvl];

var droppedItems = ["compass", "key", "pencils", "food", "shoe"];

for (var i=0; i<currentMap.length; i++) {
  for (var j=0; j<currentMap[i].length; j++) {
    if (currentMap[i][j] === 1) {
      console.log("Adding....wall");
      $('#board').append('<div class="wall"></div>');
    } else {
      console.log("Adding....spaces");
      var rc = "r" + i + "c" + j;
      var dropItem = Math.random() >= 0.96;
      $('#board').append('<div class="nowall ' + rc + ' "></div>');
      if (dropItem) {
        var itemIndex = Math.floor(Math.random() * (droppedItems.length + 1));
        $('.' + rc).addClass(droppedItems[itemIndex]);
        droppedItems.splice(itemIndex, 1);
      }
    }
  }
}

$('.r1c1').addClass('purple');
$('.r18c36').addClass('closed');

var col = 1;
var row = 1;

$(document).keydown(function(e){
    if (e.keyCode == 37) {
      console.log("left");
      if (currentMap[row][col-1] === 0) {
        $('.r' + row + 'c' + col).removeClass('purple');
        col = col-1;
        $('.r' + row + 'c' + col).addClass('purple');
      }
    } else if (e.keyCode == 38) {
      console.log("up");
      if (currentMap[row-1][col] === 0) {
        $('.r' + row + 'c' + col).removeClass('purple');
        row = row-1;
        $('.r' + row + 'c' + col).addClass('purple');
      }
    } else if (e.keyCode == 39) {
      console.log("right");
      if (currentMap[row][col+1] === 0) {
        $('.r' + row + 'c' + col).removeClass('purple');
        col = col+1;
        $('.r' + row + 'c' + col).addClass('purple');
      }
    } else if (e.keyCode == 40) {
      console.log("down");
      if (currentMap[row+1][col] === 0) {
        $('.r' + row + 'c' + col).removeClass('purple');
        row = row+1;
        $('.r' + row + 'c' + col).addClass('purple');
      }
    }
    if ($('.r' + row + 'c' + col).hasClass('food')) {
      $('.r' + row + 'c' + col).removeClass('food')
    } else if ($('.r' + row + 'c' + col).hasClass('compass')) {
      $('.r' + row + 'c' + col).removeClass('compass')
    } else if ($('.r' + row + 'c' + col).hasClass('key')) {
      $('.r' + row + 'c' + col).removeClass('key')
      $('.r18c36').removeClass('closed');
      $('.r18c36').addClass('open');
    } else if ($('.r' + row + 'c' + col).hasClass('shoe')) {
      $('.r' + row + 'c' + col).removeClass('shoe')
    } else if ($('.r' + row + 'c' + col).hasClass('pencils')) {
      $('.r' + row + 'c' + col).removeClass('pencils')
    } else if ($('.r' + row + 'c' + col).hasClass('open') && !isFinished) {
      isFinished = true;
      alert("Yay! You win!");
    }
});
